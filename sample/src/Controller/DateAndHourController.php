<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

class DateAndHourController extends AbstractController
{
    /**
     * @Route("/date-and-time", name="date_and_hour")
     */
//    public function index()
//    {
//        return $this->render('date_and_hour/index.html.twig', [
//            'controller_name' => 'DateAndHourController',
//        ]);
//    }

    public function dateAndTime() {
        date_default_timezone_set('Europe/Bucharest');
        $date = date('d-m-Y H:i:s');
        return $this->render('date_and_hour/index.html.twig', ['date'=>$date]);

    }
}
